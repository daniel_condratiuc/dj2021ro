import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class MYJDBC {
    public static void main(String[] args) {
        try {
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/dj2021ro", "root", "thebest123");
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from student");
            while (resultSet.next()) {
                System.out.print(resultSet.getString("ID")+ "  ");
                System.out.print(resultSet.getString("Prenume")+ "  ");
                System.out.print(resultSet.getString("Familia")+ "  ");
                System.out.println(resultSet.getString("Birthday")+ "  ");
//                System.out.println("\n");
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }

}